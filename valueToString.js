function toTime(seconds) {
    let a = Math.trunc(seconds / 60);
    let result;

    if (a >= 60) {
        let b = Math.trunc(a / 60);
        let c = Math.trunc(a % 60);
        result = (b + " hour(s) and " + c + " minute(s)");
    } else {
        result = ("0 hour(s) and " + a + " minute(s)");
    }
    return result;
}

console.log(toTime(3600));
console.log(toTime(3601));
console.log(toTime(3500));
console.log(toTime(323500));
console.log(toTime(0));